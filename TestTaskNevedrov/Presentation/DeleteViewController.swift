//
//  DeleteViewController.swift
//  TestTaskNevedrov
//
//  Created by Kyryl Nevedrov on 1/17/20.
//  Copyright © 2020 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class DeleteViewController: UIViewController {
    @IBOutlet weak var deletedObjectsLabel: UILabel!
    
    private let deleteOperation = DeleteOperation()
    private var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // create timer to update label every 1 second
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (timer) in
            guard let self = self else { return }
            self.deletedObjectsLabel.text = "Objects deleted: \(self.deleteOperation.objectsDeleted)"
        }
        //create queue and start delete operation
        let queue = OperationQueue()
        queue.addOperation(deleteOperation)
    }
    
    @IBAction func closeButtonDidTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // stop timer and cancel delete operation
    deinit {
         timer.invalidate()
         deleteOperation.cancel()
     }
}
