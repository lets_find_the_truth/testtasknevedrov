//
//  AddViewController.swift
//  TestTaskNevedrov
//
//  Created by Kyryl Nevedrov on 1/17/20.
//  Copyright © 2020 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {
    var addOperation = AddOperation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //create queue and start add operation
        let queue = OperationQueue()
        queue.addOperation(addOperation)
    }
}
