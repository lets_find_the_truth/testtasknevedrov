//
//  AddOperation.swift
//  TestTaskNevedrov
//
//  Created by Kyryl Nevedrov on 1/17/20.
//  Copyright © 2020 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import UIKit.UIImage

class AddOperation: Operation {
    override func main() {
        super.main()
        while true {
            // create data object around 10 MB
            let largeImage = UIImage(named: "10MB")!
            let largeObject = largeImage.jpegData(compressionQuality: 0.1)!
            // sleep thread for 100-500 ms
            let interval = Double.random(in: 0.1...0.5)
            Thread.sleep(forTimeInterval: interval)
            // add object
            SafeCollection.shared.add(object: largeObject)
        }
    }
}
