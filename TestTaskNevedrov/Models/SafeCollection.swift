//
//  SafeCollection.swift
//  TestTaskNevedrov
//
//  Created by Kyryl Nevedrov on 1/17/20.
//  Copyright © 2020 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class SafeCollection {
    // Singelton
    static let shared = SafeCollection()
    private init() {}
    // Collection
    private var array: [Data] = []
    // Queue for safe access
    private let isolationQueue = DispatchQueue(label: "com.nevedrov.isolation", attributes: .concurrent)
    
    func add(object: Data) {
        isolationQueue.async(flags: .barrier) { [weak self] in
            guard let self = self else { return }
            self.array.append(object)
        }
    }
    
    func removeAllObjects(completion: @escaping (Int) -> ()) {
        isolationQueue.async(flags: .barrier) { [weak self] in
            guard let self = self else { return }
            completion(self.array.count)
            self.array.removeAll()
        }
    }
}
