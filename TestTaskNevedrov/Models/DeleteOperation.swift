//
//  DeleteOperation.swift
//  TestTaskNevedrov
//
//  Created by Kyryl Nevedrov on 1/17/20.
//  Copyright © 2020 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class DeleteOperation: Operation {
    var objectsDeleted: Int = 0
    
    override func main() {
        super.main()
        while true {
            // break operation if it was cancelled
            guard !isCancelled else {
                break
            }
            // remove all objects
            SafeCollection.shared.removeAllObjects() { [weak self] (count) in
                guard let self = self else { return }
                self.objectsDeleted += count
            }
        }
    }
}
